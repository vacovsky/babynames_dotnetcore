# Baby Name Frequency Chart Generator #

To start this web application locally (requires DotNet Core 1 and ASP.Net 5), navigate to the root directory and execute:
```
dnu restore; dnu build;  dnx web
```

In a web browser, navigate to http://localhost:5000/BabyNames to see the results.

DotNet Core 1.0: https://github.com/aspnet/home
ASP.Net 5 setup on Mac: https://docs.asp.net/en/latest/getting-started/installing-on-mac.html
