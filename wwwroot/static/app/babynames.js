(function() {
    var app = angular.module('babynames', ['ngCookies', 'chart.js']);
    app.Root = '/';
    app.config(['$interpolateProvider',
        function($interpolateProvider) {
            $interpolateProvider.startSymbol('{[');
            $interpolateProvider.endSymbol(']}');
        }
    ]);	
    function run($rootScope, $location, $http, $scope) {}

    app.controller('BabyNamesControl', function ($rootScope, $scope, $http, $timeout, $cookies) {
        $scope.queryTime = undefined;
        $scope.showCharts = true;
        $scope.add_this_name = null;
        $scope.genderSelection = 'male';
        $scope.year = 1920;
        $scope.displayingForNames = [];
        $scope.showYear = true;
	      $scope.locale = "US";
        $scope.clearNameBoxOnSubmit = false;
	      $scope.locales = ['US','AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID',
                          'IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT',
                          'NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD',
                          'TN','TX','UT','VT','VA','WA','WV','WI','WY'];
	      $scope.oopsMessage = undefined;
        $scope.names_data = {
            names: []
        },
        $scope.add_name = function() {
            if ($scope.chartSeries.indexOf($scope.add_this_name + " (" + $scope.genderSelection.charAt(0) + "-" + $scope.locale + ")  ") > -1) {
                $scope.oopsMessage = "This name/gender/region combination has already been added to the chart.";
            } else {
                $scope.getNameData($scope.add_this_name);
                $scope.names_data.names.push($scope.add_this_name + " (" + $scope.genderSelection.charAt(0) + "-" + $scope.locale + ")  ");

                if ($scope.clearNameBoxOnSubmit) {
                    $scope.add_this_name = null;
                }
                $scope.toggleShowYear(false);
                $scope.oopsMessage = undefined;
            }
        },
        $scope.toggleCharts = function() {
            $scope.showCharts = !$scope.showCharts;
        },

        $scope.removeName = function(name) {
            var index = $scope.names_data.names.indexOf(name);

            if (index > -1) {
                $scope.chartSeries.splice(index, 1);
                $scope.chartLabels.splice(index, 1);
                $scope.chartData.splice(index, 1);
                $scope.names_data.names.splice(index, 1);
            }

            if ($scope.chartData.length === 0) {
                $scope.toggleShowYear(true);
            }
        };

        $scope.colors = [{
            fillColor: 'rgba(47, 132, 71, 0.8)',
            strokeColor: 'rgba(47, 132, 71, 0.8)',
            highlightFill: 'rgba(47, 132, 71, 0.8)',
            highlightStroke: 'rgba(47, 132, 71, 0.8)'
        }];
        
        $scope.options = {
            animation: false,
            showScale: true,
            showTooltips: true,
            pointDot: false,
            datasetStrokeWidth: 2.0,
            showLegend: true,
            interactivityEnabled: false,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: false,
            datasetFill: true,
            scaleOverride: true,
            scaleStartValue: 0,
            scaleStepWidth: .5,
            scaleSteps: 15,
            responsive: true,
            maintainAspectRatio: true,
            
        };

        //add new array to add another data set to the chart
        $scope.chartData = [];
        //years by 10?
        $scope.chartLabels = [];
        //used in legend?
        $scope.chartSeries = [];

        $scope.resetChart = function() {
            $scope.chartSeries = [];
            $scope.chartLabels = [];
            $scope.chartData = [];
            $scope.names_data.names = [];
            $scope.toggleShowYear(true);
            $scope.oopsMessage = undefined;
        };

        $scope.toggleShowYear = function(bool) {
            $scope.showYear = bool;
        };

        $scope.getNameData = function(name) {
            var qsBuilder = "name=";
            qsBuilder += name;

            if ($scope.genderSelection === 'male') {
                qsBuilder += "&gender=M";
            } else if ($scope.genderSelection === 'female') {
                qsBuilder += "&gender=F";
            }
         
            qsBuilder += "&year=" + $scope.year;
            qsBuilder += "&locale=" + $scope.locale;

            $http.get(app.Root + 'BabyNames/NameData?' + qsBuilder)
                .success(function(data, status, headers, config) {
                    var name_freq = [];
                    var labels = [];

                    for (var key in data.YearData) {
                        if (data.YearData.hasOwnProperty(key)) {
                            labels.push(parseInt(key));
                        }
                        name_freq.push(data.YearData[parseInt(key)]);
                    }
                    $scope.queryTime = data.QueryTime;
                    $scope.chartData.push(name_freq);
                    $scope.chartSeries.push(data.Name + " (" + $scope.genderSelection.charAt(0) + "-" + $scope.locale + ")  ");
                    $scope.chartLabels = labels;
                })
                .error(function(data, status, headers, config) {});
        };
    });
})();
