using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;
using babynames_dnc.Interfaces;
using babynames_dnc.Models;
using Microsoft.Extensions.OptionsModel;
using System;

namespace babynames_dnc.Controllers
{
    public class BabyNamesController : Controller
    {
        private readonly IBabyNamesRepository _babyNamesrepo;
        private readonly AppSettings _appSettings;
        public BabyNamesController(IBabyNamesRepository babyNamesrepo, IOptions<AppSettings> appSettings)
        {
            _babyNamesrepo = babyNamesrepo;
            _appSettings = appSettings.Value;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        /// JSON result showing name data by year
        /// http://localhost:5000/BabyNames/NameData?name=Joseph&gender=M&year=1900
        ///
        public IActionResult NameData(string name, string gender, int year, string locale)
        {
            var started = DateTime.Now;
            var results = _babyNamesrepo.FindNameFrequency(name, gender, year, locale);
            var ended = DateTime.Now;
            results.QueryTime = ended - started;
            return Content(JsonConvert.SerializeObject(results), "application/json");
        }

        /// View appsettings.json AppSettings section as configured in Startup.cs
        /// http://localhost:5000/BabyNames/Settings
        ///
        public IActionResult Settings()
        {
            return Content(JsonConvert.SerializeObject(_appSettings), "application/json");
        }
        
        /// 
        /// http://localhost:5000/BabyNames/GetLocaleTotals?gender=M&year=1900&locale=US
        ///
        public IActionResult GetLocaleTotals(string gender, int year, string locale)
        {
            return Content(JsonConvert.SerializeObject(_babyNamesrepo.GetLocaleTotals(gender, year, locale)), "application/json");
        }
    }
}
