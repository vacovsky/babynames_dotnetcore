using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.OptionsModel;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using babynames_dnc.Interfaces.DataAccess;
using babynames_dnc.Implementations.DataAccess;
using babynames_dnc.Interfaces;
using babynames_dnc.Implementations;
using babynames_dnc.Models;
using StackExchange.Redis;

namespace babynames_dnc
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            
        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            
            /// my own awesome services can be added here!
            services.AddTransient<IBabyNamesDataAccess, BabyNamesDataAccess>();
            services.AddTransient<IBabyNamesRepository, BabyNamesRepository>();
            services.AddTransient<ICacheManager, CacheManager>();
            //services.AddTransient<IConnectionMultiplexer, ConnectionMultiplexer>();
            
            ///to enable configuration values from appsettings.json, add conifg service below.
            ///Using the generic with the model AppSettings binds the AppSettings section to the model.
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
            }

            app.UseIISPlatformHandler();

            app.UseStaticFiles();
            
            var appSettings = app.ApplicationServices.GetService<IOptions<AppSettings>>();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=BabyNames}/{action=Index}/{id?}");
            });
            
        }

        // Entry point for the application.
        public static void Main(string[] args) => Microsoft.AspNet.Hosting.WebApplication.Run<Startup>(args);
    }
}
