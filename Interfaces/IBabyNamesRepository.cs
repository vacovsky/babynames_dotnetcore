using babynames_dnc.Models;
using System.Collections.Generic;

namespace babynames_dnc.Interfaces
{
    public interface IBabyNamesRepository
    {
        NameInfo FindNameFrequency(string name, string gender, int startYear, string locale);
        Dictionary<int, int> GetLocaleTotals(string gender, int year, string locale);
    }
}