using System.Collections.Generic;
using Microsoft.Data.Sqlite;

namespace babynames_dnc.Interfaces.DataAccess
{
    public interface IBabyNamesDataAccess
    {
        Dictionary<int, int> SumGenderLocales(int startYear, string locale, string gender);
    }
}