namespace babynames_dnc.Interfaces.DataAccess
{
    public interface ICacheManager
    {
        object GetOrAddWithExpiration(object obj, int timeout);
        object RedisGetOrAdd(string key, object obj = null);
    }
}
