namespace babynames_dnc.Models
{
    public class AppSettings
    {
        public bool DebugEnabled { get; set; }
        public string ConnectionString { get; set; }
        public string RedisConnectionString { get; set; }
        public string RabbitMQConnectionString { get; set; }
    }
}