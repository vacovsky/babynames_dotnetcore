using System;
using System.Collections.Generic;

namespace babynames_dnc.Models
{
    public class NameInfo
    {
        public string Name { get; set; }

        public Dictionary<int, float> YearData { get; set; }

        public string Gender { get; set; }

        public TimeSpan QueryTime { get; set; }
    }
}