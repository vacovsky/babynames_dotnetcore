using babynames_dnc.Interfaces.DataAccess;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Sqlite;
using babynames_dnc.Models;
using Microsoft.Extensions.OptionsModel;


namespace babynames_dnc.Implementations.DataAccess
{
    public class BabyNamesDataAccess : IBabyNamesDataAccess
    {
        private readonly ICacheManager _cacheManager;
        private readonly AppSettings _AppSettings;
        private static string _DbName;
        public BabyNamesDataAccess(IOptions<AppSettings> appSettings, ICacheManager cacheManager)
        {
            _AppSettings = appSettings.Value;
            _DbName = _AppSettings.ConnectionString;
            _cacheManager = cacheManager;
        }

        public Dictionary<int, int> SumGenderLocales(int startYear, string locale, string gender)
        {
            var finalResult = new Dictionary<int, int>();
            foreach (var year in Enumerable.Range(startYear, 2015 - startYear))
            {
                var cacheKey = String.Format("{0}:{1}:{2}", gender.ToUpper(), locale.ToUpper(), year);
                var val = _cacheManager.RedisGetOrAdd(cacheKey);

                var cachedValue = Int32.Parse(val.ToString());
                if (val.ToString() != "")
                {
                    finalResult[year] = (int)cachedValue;
                }
                else
                {
                    string sqlStr;
                    if (locale.ToUpper() == "US")
                    {
                        sqlStr = String.Format(
                            "SELECT SUM(Count) FROM NationalNames WHERE Year = {0} AND gender = '{1}'",
                            year,
                            gender);
                    }
                    else
                    {
                        sqlStr = String.Format(
                            "SELECT SUM(Count) FROM StateNames WHERE Year = {0} AND gender = '{1}' AND State = '{2}'",
                            year,
                            gender,
                            locale
                            );
                    }

                    finalResult[year] = RunCalcQuery(sqlStr);
                    _cacheManager.RedisGetOrAdd(cacheKey, finalResult[year].ToString());
                }
            }
            return finalResult;
        }



        public int RunCalcQuery(string sqlStr)
        {

            int total = 0;

            var resultsSet = new Dictionary<string, float>();

            using (var connection = new SqliteConnection("" +
                    new SqliteConnectionStringBuilder
                    {
                        DataSource = _DbName
                    }))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    var finalResult = new Dictionary<string, int>();
                    var selectCommand = connection.CreateCommand();
                    selectCommand.Transaction = transaction;
                    selectCommand.CommandText = sqlStr;
                    using (var reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            total = Int32.Parse(reader.GetString(0));
                        }
                    }
                }
            }
            return total;
        }

    }
}