using System;
using System.Collections;
using System.Linq;
using StackExchange.Redis;
using babynames_dnc.Interfaces.DataAccess;
using Newtonsoft.Json;
using Microsoft.Extensions.OptionsModel;
using babynames_dnc.Models;

namespace babynames_dnc.Implementations.DataAccess
{
    public class CacheManager : ICacheManager
    {
        private static ConnectionMultiplexer _redis;
        private static AppSettings _AppSettings;
        private readonly IDatabase _redisDb;

        public CacheManager(IOptions<AppSettings> appSettings)
        {
            _AppSettings = appSettings.Value;
            _redis = ConnectionMultiplexer.Connect(_AppSettings.RedisConnectionString);
            _redisDb = _redis.GetDatabase();

        }

        public object GetOrAddWithExpiration(object obj, int timeout)
        {
            throw new NotImplementedException();
        }

        public object RedisGetOrAdd(string key, object obj = null)
        {
            try
            {
                var storedObj = _redisDb.StringGet(key);
                if (storedObj.IsNull)
                {
                    if (obj != null)
                    {
                        try
                        {
                            _redisDb.StringSet(key, (string)obj);
                        }
                        catch
                        {
                            _redisDb.StringSet(key, JsonConvert.SerializeObject(obj));
                        }
                        return obj;
                    }
                    else
                    {
                        return obj;
                    }
                }
                else
                {
                    return storedObj;
                }
            }
            catch
            {
                if (_AppSettings.DebugEnabled)
                {
                    Console.WriteLine(_redis.IsConnected);
                    Console.WriteLine($"Redis failure: {key} - fall back to SQL");
                }
                return obj;
            }
        }
    }
}
