using System;
using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System.Linq;
using babynames_dnc.Models;
using babynames_dnc.Interfaces;
using Microsoft.Extensions.OptionsModel;
using babynames_dnc.Interfaces.DataAccess;

namespace babynames_dnc.Implementations
{
    public class BabyNamesRepository : IBabyNamesRepository
    {
        private readonly AppSettings _AppSettings;
        private readonly IBabyNamesDataAccess _babyData;

        private readonly bool _Debug;
        private static string _DbName;

        public BabyNamesRepository(IOptions<AppSettings> appSettings, IBabyNamesDataAccess babyData)
        {
            _AppSettings = appSettings.Value;
            _Debug = _AppSettings.DebugEnabled;
            _DbName = _AppSettings.ConnectionString;
            _babyData = babyData;
        }

        public NameInfo FindNameFrequency(string name, string gender, int startYear, string locale)
        {
            string something;
            double totalForYear = 0;
            string sqlStr;

            if (locale != "US")
            {
                sqlStr = string.Format(
                @"SELECT Year, Count FROM NationalNames WHERE Year>={0} 
                AND Gender ='{1}' AND Name='{2}'  ORDER BY Year ASC",
                startYear,
                gender,
                name);
                // SELECT SUM(Count) FROM nationalnames WHERE year = 2012 AND gender="M"
            }
            else
            {
                sqlStr = string.Format(
                @"SELECT Year, Count FROM NationalNames WHERE Year>={0} 
                AND Gender ='{1}' AND Name='{2}' ORDER BY Year ASC",
                startYear,
                gender,
                name);
            }
            
            var totalsData = _babyData.SumGenderLocales(startYear, locale, gender);
            var resultsSet = new Dictionary<int, float>();

            using (var connection = new SqliteConnection("" +
                    new SqliteConnectionStringBuilder
                    {
                        DataSource = _DbName
                    }))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    var finalResult = new Dictionary<string, int>();
                    var selectCommand = connection.CreateCommand();
                    selectCommand.Transaction = transaction;
                    selectCommand.CommandText = sqlStr;
                    using (var reader = selectCommand.ExecuteReader())
                    {
                        var prevYear = startYear - 1;

                        while (reader.Read())
                        {
                            var year = Int32.Parse(reader.GetString(0));
                            var percent = (float.Parse(reader.GetString(1)) / totalsData[year]) * 100;

                            if (year == prevYear + 1)
                            {
                                resultsSet[year] = percent;
                                prevYear = year;
                            }
                            else if (year - prevYear > 1)
                            {
                                while (prevYear - year >= 1)
                                {
                                    prevYear++;
                                    year = prevYear;
                                    resultsSet[year] = 0.0f;
                                }
                                resultsSet[year] = percent;
                                prevYear = year;
                            }
                        }
                        if (resultsSet.Keys.Count == 0)
                        {
                            foreach (var i in Enumerable.Range(startYear, (2015 - startYear))) //DateTime.Now.Year))
                            {
                                resultsSet[i] = 0;
                            }
                        }
                        foreach (var i in Enumerable.Range(startYear, (2015 - startYear)))
                        {
                            if (!resultsSet.Keys.Contains(i))
                            {
                                resultsSet[i] = 0.0f;
                            }
                        }
                    }
                }
            }

            var finalResults = new NameInfo
            {
                Name = name,
                Gender = gender,
                YearData = new Dictionary<int, float>()
            };

            var f_result = new List<object>();
            foreach (var r in resultsSet.Keys)
            {
                finalResults.YearData[r] = resultsSet[r];
            }

            /// Debug/Config test here
            Print("Debug enabled in appsettings.json: " + _Debug);

            if (_Debug)
            {
                foreach (var x in finalResults.YearData.Keys)
                {
                    Print(x + ": " + finalResults.YearData[x]);
                }
            }
            return finalResults;
        }


        public Dictionary<int, int> GetLocaleTotals(string gender, int year, string locale)
        {
            return _babyData.SumGenderLocales(year, locale, gender);
        }


        private void Print(object message)
        {
            Console.WriteLine(message.ToString());

        }
    }
}
